# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://td8374@bitbucket.org/td8374/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/td8374/stroboskop/commits/d9c50cead5592adffa3e10dec6d4064385e04fb0

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/td8374/stroboskop/commits/5b0f0ec257d037e32737923cd8b8efd7dacf1eac

Naloga 6.3.2:
https://bitbucket.org/td8374/stroboskop/commits/a04dd0c1325313baa7e9d2f07d0005babd0c1ded

Naloga 6.3.3:
https://bitbucket.org/td8374/stroboskop/commits/fcaf303717f837cf63022a603f56a8b2bcbf0648?at=izgled

Naloga 6.3.4:
https://bitbucket.org/td8374/stroboskop/commits/35abad8238bd625dace0da3a48d543f6f6c1d1b4

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/td8374/stroboskop/commits/dcb17d2bb9a7a4d7594d034d53f8935c2b393d6b?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/td8374/stroboskop/commits/6c94a01a9d33fb153518d5a2dffb0f30a82c8dd4?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/td8374/stroboskop/commits/b3944467ad6add0e01d0e2b1e6ede2e3d4c5757c?at=dinamika

Naloga 6.4.4:
https://bitbucket.org/td8374/stroboskop/commits/de76c7f9dd2572f59c2506c90419803109059e00?at=dinamika